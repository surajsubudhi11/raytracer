//
// Created by Suraj Subudhi on 7/5/17.
//

#include "DirectionalLight.h"
#include "./../World/World.h"


DirectionalLight::DirectionalLight() : Light(), m_color(Color(1.0)), m_ls(1.0), m_direction(Vector3(0, 1, 0))
{

}

DirectionalLight::~DirectionalLight(){}

DirectionalLight::DirectionalLight(const DirectionalLight& _pl) : Light(), m_color(_pl.m_color), m_ls(_pl.m_ls), m_direction(_pl.m_direction)
{
    m_shadow = _pl.m_shadow;
}

DirectionalLight::DirectionalLight(const Color& _col, float _ls, const Vector3& _dir) : Light(), m_color(_col), m_ls(_ls), m_direction(_dir)
{

}

DirectionalLight::DirectionalLight(float _ls, const Vector3& _dir) : Light(), m_color(Color(1.0)), m_ls(_ls), m_direction(_dir)
{

}

DirectionalLight& DirectionalLight::operator=(const DirectionalLight& rhs)
{
    if(this == &rhs)
        return *this;

    Light::operator=(rhs);

    m_shadow = rhs.m_shadow;
    m_color  = rhs.m_color;
    m_ls     = rhs.m_ls;
    m_direction = rhs.m_direction;

    return *this;
}

Vector3 DirectionalLight::get_direction(ShadeData& sd)
{
    return m_direction;
}

Color DirectionalLight::irradiance(ShadeData& sd)
{
    return (m_color * m_ls);
}


bool DirectionalLight::in_shadow(const Ray& ray, const ShadeData& sr ) const
{
    double t;
    int num_of_objects = sr.w.NumOfObjects();

    for(int i = 0; i < num_of_objects; i++){
        if(sr.w.GetObjectAtIndex(i)->shadow_hit(ray, t))
            return true;
    }

    return false;
}