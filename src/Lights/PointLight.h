//
// Created by Suraj Subudhi on 7/5/17.
//

#ifndef LEARNRAYTRACER_POINTLIGHT_H
#define LEARNRAYTRACER_POINTLIGHT_H


#include "Light.h"

class PointLight : public Light
{
public:
    PointLight();
    ~PointLight();

    PointLight(const PointLight& _pl);
    PointLight(const Color& _col, float _ls, const Point3& _vec = Point3());
    PointLight(float _ls, const Point3& _vec = Point3());

    PointLight& operator=(const PointLight& rhs);

    virtual Vector3 get_direction(ShadeData& sd) override;
    virtual Color irradiance(ShadeData& sd) override;

//    bool cast_shadow();
    bool in_shadow(const Ray& ray, const ShadeData& sr ) const override;


    inline void set_color(const Color& _col){m_color = _col;}
    inline void set_ls   (const float& _ls ){m_ls = _ls;}
    inline void set_location(const Point3& _loc ){m_location = _loc;}

    inline Color get_color(){ return m_color;}
    inline float get_ls(){ return m_ls;}
    inline Point3 get_location(){ return m_location;}

private:
    Color m_color;
    float m_ls;
    Point3 m_location;

};


#endif //LEARNRAYTRACER_POINTLIGHT_H
