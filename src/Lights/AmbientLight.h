//
// Created by Suraj Subudhi on 7/5/17.
//

#ifndef LEARNRAYTRACER_AMBIENTLIGHT_H
#define LEARNRAYTRACER_AMBIENTLIGHT_H


#include "Light.h"

class AmbientLight : public Light
{
public:
    AmbientLight();
    ~AmbientLight();

    AmbientLight(const Color& _col, float _ls);
    AmbientLight(float _ls);

    AmbientLight(const AmbientLight& _amb);
    AmbientLight& operator=(const AmbientLight& rhs);

    virtual Vector3 get_direction(ShadeData& sd) override;
    virtual Color irradiance(ShadeData& sd) override;

    inline void set_color(const Color& _col){m_color = _col;}
    inline void set_ls   (const float& _ls ){m_ls = _ls;}

    inline Color get_color(){ return m_color;}
    inline float get_ls(){ return m_ls;}

private:
    Color m_color;
    float m_ls;

};


#endif //LEARNRAYTRACER_AMBIENTLIGHT_H
