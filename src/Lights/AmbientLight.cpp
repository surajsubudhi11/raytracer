//
// Created by Suraj Subudhi on 7/5/17.
//

#include "AmbientLight.h"


AmbientLight::AmbientLight(): Light(false), m_color(Color()), m_ls(1.0)
{

}

AmbientLight::~AmbientLight(){}

AmbientLight::AmbientLight(const Color& _col, float _ls): Light(false), m_color(_col), m_ls(_ls)
{

}

AmbientLight::AmbientLight(float _ls): Light(false), m_color(Color()), m_ls(_ls)
{

}

AmbientLight::AmbientLight(const AmbientLight& _amb): Light(false), m_color(_amb.m_color), m_ls(_amb.m_ls)
{
    m_shadow = _amb.m_shadow;
}

AmbientLight& AmbientLight::operator=(const AmbientLight& rhs)
{
    if(this == &rhs)
        return *this;

    m_shadow = rhs.m_shadow;
    m_color = rhs.m_color;
    m_ls = rhs.m_ls;

    return *this;
}

Vector3 AmbientLight::get_direction(ShadeData& sd)
{
    return Vector3(0.0, 0.0, 0.0);
}

Color AmbientLight::irradiance(ShadeData& sd)
{
    return (m_color * m_ls);
}

