//
// Created by Suraj Subudhi on 7/5/17.
//

#include "Light.h"

Light::Light() : m_shadow(true)
{

}

Light::~Light() {}

Light::Light(bool _shadow) : m_shadow(_shadow)
{

}

Light::Light(const Light &light) : m_shadow(light.m_shadow)
{

}

Light& Light::operator=(const Light &rhs)
{
    if(this == &rhs)
        return *this;

    m_shadow = rhs.m_shadow;
    return *this;
}

bool Light::cast_shadow()
{
    return m_shadow;
}

bool Light::in_shadow(const Ray& ray, const ShadeData& sr )const {
    return false;
}