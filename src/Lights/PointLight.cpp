//
// Created by Suraj Subudhi on 7/5/17.
//

#include "PointLight.h"
#include "./../World/World.h"

PointLight::PointLight() : Light(), m_color(Color(1.0)), m_ls(1.0), m_location(Point3())
{

}

PointLight::~PointLight(){}

PointLight::PointLight(const PointLight& _pl) : Light(), m_color(_pl.m_color), m_ls(_pl.m_ls), m_location(_pl.m_location)
{
    m_shadow = _pl.m_shadow;
}

PointLight::PointLight(const Color& _col, float _ls, const Point3& _vec) : Light(), m_color(_col), m_ls(_ls), m_location(_vec)
{

}

PointLight::PointLight(float _ls, const Point3& _vec) : Light(), m_color(Color(1.0)), m_ls(_ls), m_location(_vec)
{

}

PointLight& PointLight::operator=(const PointLight& rhs)
{
    if(this == &rhs)
        return *this;

    m_shadow = rhs.m_shadow;
    m_color  = rhs.m_color;
    m_ls     = rhs.m_ls;
    m_location = rhs.m_location;

    return *this;
}

Vector3 PointLight::get_direction(ShadeData& sd)
{
    Vector3 temp = (m_location - sd.local_hit_point);
    return (temp.hat());
}

Color PointLight::irradiance(ShadeData& sd)
{
    // Note : distance attenuation not used.
    return (m_color * m_ls);
}



bool PointLight::in_shadow(const Ray& ray, const ShadeData& sr ) const
{
    double t;
    int num_of_objects = sr.w.NumOfObjects();
    //Point3 rayOrigin = ray.origin.add(Vector3(0, 0.1, 0));
    double d = m_location.distance(ray.origin);
    //double d = m_location.distance(rayOrigin);

    for(int i = 0; i < num_of_objects; i++){
        if(sr.w.GetObjectAtIndex(i)->shadow_hit(ray, t) && t < d) {
            return true;
        }
    }

    return false;
}