//
// Created by Suraj Subudhi on 7/5/17.
//

#ifndef LEARNRAYTRACER_DIRECTIONALLIGHT_H
#define LEARNRAYTRACER_DIRECTIONALLIGHT_H


#include "Light.h"

class DirectionalLight : public Light
{
public:
    DirectionalLight();
    ~DirectionalLight();

    DirectionalLight(const DirectionalLight& _pl);
    DirectionalLight(const Color& _col, float _ls, const Vector3& _dir = Vector3());
    DirectionalLight(float _ls, const Vector3& _dir = Vector3());

    DirectionalLight& operator=(const DirectionalLight& rhs);

    bool in_shadow(const Ray& ray, const ShadeData& sr ) const override;

    virtual Vector3 get_direction(ShadeData& sd) override;
    virtual Color irradiance(ShadeData& sd) override;


    inline void set_color(const Color& _col){m_color = _col;}
    inline void set_ls   (const float& _ls ){m_ls = _ls;}
    inline void set_direction(const Vector3& _dir )
    {
        Vector3 normDir = _dir;
        normDir.normalized();
        m_direction = normDir;
    }

    inline Color get_color(){ return m_color;}
    inline float get_ls(){ return m_ls;}

private:
    Color m_color;
    float m_ls;
    Vector3 m_direction;
};


#endif //LEARNRAYTRACER_DIRECTIONALLIGHT_H
