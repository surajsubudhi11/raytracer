//
// Created by Suraj Subudhi on 7/5/17.
//

#ifndef LEARNRAYTRACER_LIGHT_H
#define LEARNRAYTRACER_LIGHT_H


#include "../Utilites/Vector3.h"
#include "../Utilites/ShadeData.h"

class Light {

public:
    Light();
    virtual ~Light();

    Light(bool _shadow);
    Light(const Light& light);

    Light& operator=(const Light& rhs);

    virtual Vector3 get_direction(ShadeData& sd) = 0;
    virtual Color   irradiance(ShadeData& sd) = 0;

    bool cast_shadow();
    virtual bool in_shadow(const Ray& ray, const ShadeData& sr ) const;

protected:
    bool m_shadow;

};


#endif //LEARNRAYTRACER_LIGHT_H
