//
// Created by Suraj Subudhi on 7/8/17.
//

#ifndef LEARNRAYTRACER_PHONG_H
#define LEARNRAYTRACER_PHONG_H


#include "Materials.h"
#include "../BRDF/Lambertian.h"
#include "../BRDF/Glossy.h"

class Phong : public Materials
{
public:
    Phong();
    ~Phong();

    Phong(const Phong& ph);
    Phong& operator=(const Phong& rhs);

    Color RaycastShade(ShadeData& sd) override;

    inline void set_ka(const float k){ambient_ptr->set_kd(k);}
    inline void set_kd(const float k){diffuse_ptr->set_kd(k);}
    inline void set_ks(const float k){specular_ptr->set_ks(k);}

    inline void set_cd(const Color& col){ambient_ptr->set_cd(col); diffuse_ptr->set_cd(col);}
    inline void set_cs(const Color& col, const float e){specular_ptr->set_cs(col); specular_ptr->set_exp(e);}

private:
    Lambertian* ambient_ptr;
    Lambertian* diffuse_ptr;
    Glossy*     specular_ptr;

};


#endif //LEARNRAYTRACER_PHONG_H
