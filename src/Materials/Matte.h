//
// Created by suraj on 7/5/17.
//

#ifndef LEARNRAYTRACER_MATTE_H
#define LEARNRAYTRACER_MATTE_H


#include "Materials.h"
#include "../BRDF/Lambertian.h"

class Matte : public Materials
{
public:
    Matte();
    ~Matte();

    Matte(const Matte& mat);

    Color RaycastShade(ShadeData& sd) override;

    inline void set_ka(const float k){ambient_ptr->set_kd(k);}
    inline void set_kd(const float k){diffuse_ptr->set_kd(k);}
    inline void set_cd(const Color& col){ambient_ptr->set_cd(col); diffuse_ptr->set_cd(col);}

private:
    Lambertian* ambient_ptr;
    Lambertian* diffuse_ptr;
};


#endif //LEARNRAYTRACER_MATTE_H
