//
// Created by Suraj Subudhi on 7/8/17.
//

#include "Phong.h"
#include "../World/World.h"

Phong::Phong() : Materials(),
                 ambient_ptr(new Lambertian()),
                 diffuse_ptr(new Lambertian()),
                 specular_ptr(new Glossy())
{

}

Phong::~Phong()
{
    ambient_ptr = nullptr;
    diffuse_ptr = nullptr;
    specular_ptr = nullptr;
}

Phong::Phong(const Phong& ph) : Materials(),
                                ambient_ptr(ph.ambient_ptr),
                                diffuse_ptr(ph.diffuse_ptr),
                                specular_ptr(ph.specular_ptr)
{

}

Phong& Phong::operator=(const Phong& rhs)
{
    if(this == &rhs)
        return *this;

    Materials::operator=(rhs);

    ambient_ptr = rhs.ambient_ptr;
    diffuse_ptr = rhs.diffuse_ptr;
    specular_ptr = rhs.specular_ptr;

    return *this;
}

Color Phong::RaycastShade(ShadeData& sd)
{
    // wo: direction from the hit point to the primary ray origin.
    Vector3 wo = sd.ray.direction;
    wo.z = -1 * wo.z;
    Color irrad = ambient_ptr->reflectance(sd, wo) * sd.w.GetAmbientLightPtr()->irradiance(sd);
    irrad = irrad.max_to_one();
    int num_of_lights = sd.w.NumOfLights();

//    std::cout << "Num of Lights : " << num_of_lights << std::endl;

    for (int i = 0; i < num_of_lights; i++) {
        // wi : direction from the hit point to the light.
        Vector3 wi = sd.w.GetLightAtIndex(i)->get_direction(sd);

        //wi = wi.negate();
        double ndotwi = sd.normal.dot(wi);

        // TODO Finding bug here Analyzing the direction and hit point data.

        if(ndotwi > 0.0) {
            bool in_shadow = false;
            if(sd.w.GetLightAtIndex(i)->cast_shadow()){
                Ray shadowRay(sd.local_hit_point, wi);
                shadowRay.origin = sd.local_hit_point;
                shadowRay.direction = wi;
                in_shadow = sd.w.GetLightAtIndex(i)->in_shadow(shadowRay, sd);
            }

            if(!in_shadow) {

                irrad += ((diffuse_ptr->brdf_func(sd, wo, wi) + specular_ptr->brdf_func(sd, wo, wi)) *
                          sd.w.GetLightAtIndex(i)->irradiance(sd)) * (float) ndotwi;
            }
        }
    }

    return irrad;

}