//
// Created by Suraj Subudhi on 7/5/17.
//

#ifndef LEARNRAYTRACER_MATERIALS_H
#define LEARNRAYTRACER_MATERIALS_H


#include "../Utilites/Color.h"
//#include "../Utilites/ShadeData.h"
class ShadeData;

class Materials {

public:
    Materials();
    virtual ~Materials();

    Materials(const Materials& mat);
    Materials& operator=(const Materials& rhs);

    virtual Color RaycastShade(ShadeData& sd);

};


#endif //LEARNRAYTRACER_MATERIALS_H
