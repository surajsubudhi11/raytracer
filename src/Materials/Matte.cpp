//
// Created by Suraj Subudhi on 7/5/17.
//

#include "Matte.h"
#include "../World/World.h"


Matte::Matte(): Materials(), ambient_ptr(new Lambertian()), diffuse_ptr(new Lambertian())
{

}

Matte::~Matte()
{
    ambient_ptr = nullptr;
    diffuse_ptr = nullptr;
}

Matte::Matte(const Matte& mat) : ambient_ptr(mat.ambient_ptr), diffuse_ptr(mat.diffuse_ptr)
{

}

Color Matte::RaycastShade(ShadeData& sd)
{
    Vector3 wo = sd.ray.direction * -1;
    Color irrad = ambient_ptr->reflectance(sd, wo) * sd.w.GetAmbientLightPtr()->irradiance(sd);
    int num_of_lights = sd.w.NumOfLights();

    for (int i = 0; i < num_of_lights; i++) {
        Vector3 wi = sd.w.GetLightAtIndex(i)->get_direction(sd);
        double ndotwi = sd.normal * wi;

        if (ndotwi > 0.0) {
            bool in_shadow = false;
            if(sd.w.GetLightAtIndex(i)->cast_shadow()){
                //Vector3 shadowDirection = sd.w.GetLightAtIndex(i)
                Ray shadowRay(sd.local_hit_point, wi);
                shadowRay.origin = sd.local_hit_point;
                shadowRay.direction = wi;

                in_shadow = sd.w.GetLightAtIndex(i)->in_shadow(shadowRay, sd);
            }

            if(!in_shadow){
                irrad += diffuse_ptr->brdf_func(sd, wo, wi) * sd.w.GetLightAtIndex(i)->irradiance(sd) * (float) ndotwi;
            }
        }
    }

    return irrad;
}