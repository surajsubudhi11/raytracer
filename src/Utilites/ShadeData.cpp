//
// Created by Suraj Subudhi on 25-Jun-17.
//

#include "ShadeData.h"

ShadeData::ShadeData(World& world) : hit_an_object(false),
                                     local_hit_point(),
                                     normal(Normal()), color(Color(0)),
                                     w(world),
                                     ray(),
                                     materials_ptr(nullptr),
                                     hit_point(Point3())

{

}
ShadeData::ShadeData(const ShadeData& s) : w(s.w), materials_ptr(s.materials_ptr), hit_point(s.hit_point), ray(s.ray)
{
    hit_an_object = s.hit_an_object;
    local_hit_point = s.local_hit_point;
    normal = s.normal;
    color = s.color;
}

ShadeData::~ShadeData()
{

}

ShadeData& ShadeData::operator= (const ShadeData& rhs)
{
    if (this == &rhs)
        return (*this);

    local_hit_point = rhs.local_hit_point;
    hit_an_object   = rhs.hit_an_object;
    normal          = rhs.normal;
    color           = rhs.color;
    //w               = rhs.w;

    return (*this);
}