//
// Created by Suraj Subudhi on 24-Jun-17.
//

#include "Matrix4.h"


Matrix4::Matrix4() {
    for (int x = 0; x < 4; x++) {
        for (int y = 0; y < 4; y++)
        {
            if (x == y)
                m[x][y] = 1.0;
            else
                m[x][y] = 0.0;
        }
    }
}

Matrix4::Matrix4(const Matrix4& mat)
{
    for (int x = 0; x < 4; x++) {
        for (int y = 0; y < 4; y++)
        {
            m[x][y] = mat.m[x][y];
        }
    }
}

Matrix4::~Matrix4()
{

}

Matrix4& Matrix4::operator=(const Matrix4& rhs)
{
    if (this == &rhs)
        return (*this);

    for (int x = 0; x < 4; x++)
        for (int y = 0; y < 4; y++)
            m[x][y] = rhs.m[x][y];

    return (*this);
}

Matrix4& Matrix4::operator*(const Matrix4& mat) const
{
    Matrix4	product;

    for (int y = 0; y < 4; y++)
        for (int x = 0; x < 4; x++) {
            double sum = 0.0;

            for (int j = 0; j < 4; j++)
                sum += m[x][j] * mat.m[j][y];

            product.m[x][y] = sum;
        }

    return (product);
}

Matrix4  Matrix4::operator/(const double a)
{
    for (int x = 0; x < 4; x++)
        for (int y = 0; y < 4; y++)
            m[x][y] = m[x][y] / a;

    return (*this);
}

//void Matrix4::transpose()
//{
//
//}