//
// Created by Suraj Subudhi on 25-Jun-17.
//

#ifndef LEARNRAYTRACER_SHADEDATA_H
#define LEARNRAYTRACER_SHADEDATA_H

class World;

#include "Point3.h"
#include "Normal3.h"
#include "Color.h"
#include "Ray.h"
#include "../Materials/Materials.h"
//#include "../World/World.h"

struct SurfaceRayIntersection
{
    bool isIntersecting;
    double disFromRayOrigin;
    Point3 point;
    Normal normal;
};


class ShadeData {

public:
    ShadeData(World& wor);
    ShadeData(const ShadeData& s);
    ~ShadeData();

    ShadeData& operator= (const ShadeData& rhs);


public:
    bool hit_an_object;
    Point3 local_hit_point;
    Normal normal;
    Color color;
    World& w;

    Ray ray;
    Materials* materials_ptr;
    Point3 hit_point;

};


#endif //LEARNRAYTRACER_SHADEDATA_H
