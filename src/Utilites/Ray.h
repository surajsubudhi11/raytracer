//
// Created by Suraj Subudhi on 24-Jun-17.
//

#ifndef RAY_H
#define RAY_H

#include "Point3.h"
#include "Vector3.h"

class Ray {
public:

    Ray();
    Ray(const Point3& _orig, const Vector3& _dir);
    Ray(const Ray& ray);
    ~Ray();

    Ray& operator= (const Ray& rhs);

    Point3 origin;
    Vector3 direction;

};


#endif //RAY_H
