//
// Created by Suraj Subudhi on 24-Jun-17.
//
#ifndef LEARNRAYTRACER_VECTOR3_H
#define LEARNRAYTRACER_VECTOR3_H

#include <math.h>
#include <iostream>
#include <assert.h>
//#include "Normal3.h"

//#include "Normal3.h"
//#include "Point3.h"

class Normal;

class Vector3 {
public:
    Vector3();
    Vector3(double _x, double _y, double _z);
    Vector3(const Vector3& _vec);
    ~Vector3();

    Vector3 add(const Vector3& _vec) const;
    Vector3 sub(const Vector3& _vec) const;
    Vector3 mult(const double _a) const;
    Vector3 div(const double _a) const;

    double squaredLength() const;
    double length() const;

    Vector3 negate();
    void normalized();

    Vector3& hat(void);

    double dot(const Vector3& _vec) const;
    double dot(const Normal& _vec) const;
    Vector3 cross(const Vector3& _vec) const;

    // Operators

    Vector3& operator= (const Vector3& rhs);
    //Vector3& operator= (const Normal& rhs);
    //Vector3& operator= (const Point3& rhs);

    Vector3 operator+ (const Vector3& v) const;
    //Vector3 operator+ (const Normal& n);
    Vector3 operator- (const Vector3& v) const;
    double  operator* (const Vector3& v) const;
    Vector3 operator* (double a) const;
    Vector3 operator/ (double a) const;


    inline Vector3 operator^ (const Vector3& v) const {
        return cross(v);
    }

    inline Vector3& operator+= (const Vector3& v) {
        x += v.x; y += v.y; z += v.z;
        return (*this);
    }

    double x, y, z;

};


#endif //LEARNRAYTRACER_VECTOR3_H
