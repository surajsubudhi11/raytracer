//
// Created by Suraj Subudhi on 24-Jun-17.
//

#ifndef LEARNRAYTRACER_MATHS_H
#define LEARNRAYTRACER_MATHS_H

#include <stdlib.h>

const double 	PI 			= 3.1415926535897932384;
const double 	TWO_PI 		= 6.2831853071795864769;
const double 	PI_ON_180 	= 0.0174532925199432957;
const double 	invPI 		= 0.3183098861837906715;
const double 	invTWO_PI 	= 0.1591549430918953358;

const float 	invRAND_MAX = 1.0 / (float)RAND_MAX;


inline double min(double x0, double x1)
{
    return ((x0 < x1) ? x0 : x1);
}

inline double max(double x0, double x1)
{
    return ((x0 > x1) ? x0 : x1);
}

inline int rand_int(void)
{
    return(rand());
}

inline float rand_float(void)
{
    return((float)rand_int() * invRAND_MAX);
}

inline void set_rand_seed(const int seed)
{
    srand(seed);
}

inline float rand_float(int l, float h)
{
    return (rand_float() * (h - l) + l);
}

inline int rand_int(int l, int h)
{
    return ((int) (rand_float(0, h - l + 1) + l));
}

inline double clamp(const double x, const double min, const double max)
{
    return (x < min ? min : (x > max ? max : x));
}

int SolveQuadric(double c[3], double s[2]);

int SolveCubic(double c[4], double s[3]);

int SolveQuartic(double c[5], double s[4]);

#endif //LEARNRAYTRACER_MATHS_H
