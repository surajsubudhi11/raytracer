//
// Created by suraj on 6/26/17.
//

#ifndef LEARNRAYTRACER_POINT2_H
#define LEARNRAYTRACER_POINT2_H

#include <math.h>

class Point2 {
public:
    Point2();
    Point2(const double _a);
    Point2(const double _x, const double _y);
    Point2(const Point2& _p);
    ~Point2();

    Point2 mult(const double _a) const;
    Point2 div(const double _a) const;

    double squaredDistance(const Point2& p) const;
    double distance(const Point2& p) const;


    Point2& operator= (const Point2& rhs);
    Point2 operator* (const double _a) const;
    Point2 operator/ (const double _a) const;


    double x, y;
};


#endif //LEARNRAYTRACER_POINT2_H
