//
// Created by Suraj Subudhi on 24-Jun-17.
//

#ifndef LEARNRAYTRACER_POINT3_H
#define LEARNRAYTRACER_POINT3_H

#include "Vector3.h"

class Point3 {

public:
    Point3();
    Point3(const double _a);
    Point3(const double _x, const double _y, const double _z);
    Point3(const Point3& _p);
    ~Point3();

    Point3 add(const Vector3& _vec) const;
    Point3 sub(const Vector3& _vec) const;
    Vector3 sub(const Point3& _p) const;
    Point3 mult(const double _a) const;
    Point3 div(const double _a) const;

    double squaredDistance(const Point3& p) const;
    double distance(const Point3& p) const;


    Point3& operator= (const Point3& rhs);
    Vector3 operator- (const Point3& p) const;
    Point3 operator- (const Vector3& _vec) const;
    Point3 operator+ (const Vector3& _vec) const;
    Point3 operator* (const double _a) const;
    Point3 operator/ (const double _a) const;


    double x, y, z;
};


#endif //LEARNRAYTRACER_POINT3_H
