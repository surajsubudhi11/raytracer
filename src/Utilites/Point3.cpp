//
// Created by Suraj Subudhi on 24-Jun-17.
//

#include "Point3.h"


Point3::Point3() : x(0), y(0), z(0)
{

}

Point3::Point3(const double _a): x(_a), y(_a), z(_a)
{

}

Point3::Point3(const double _x, const double _y, const double _z): x(_x), y(_y), z(_z)
{

}

Point3::Point3(const Point3& _p)
{
    x = _p.x;
    y = _p.y;
    z = _p.z;
}

Point3::~Point3(){}


Point3 Point3::add(const Vector3& _vec) const
{
    return Point3(x + _vec.x, y + _vec.y, z + _vec.z);
}

Point3 Point3::sub(const Vector3& _vec) const
{
    return Point3(x - _vec.x, y - _vec.y, z - _vec.z);
}

Vector3 Point3::sub(const Point3& _p) const
{
    return Vector3(x - _p.x, y - _p.y, z - _p.z);
}

Point3 Point3::mult(const double _a) const
{
    return Point3(x * _a , y * _a, z * _a);
}

Point3 Point3::div(const double _a) const
{
    return Point3(x / _a , y / _a, z / _a);
}

double Point3::squaredDistance(const Point3& p) const
{
    Vector3 disVec = sub(p);
    return disVec.squaredLength();
}

double Point3::distance(const Point3& p) const
{
    Vector3 disVec = sub(p);
    return disVec.length();
}

Point3& Point3::operator= (const Point3& rhs)
{
    if (this == &rhs)
        return (*this);

    x = rhs.x; y = rhs.y; z = rhs.z;
    return (*this);
}

Vector3 Point3::operator- (const Point3& p) const
{
    return sub(p);
}

Point3 Point3::operator- (const Vector3& _vec) const
{
    return sub(_vec);
}

Point3 Point3::operator+ (const Vector3& _vec) const
{
    return add(_vec);
}

Point3 Point3::operator* (const double _a) const
{
    return mult(_a);
}

Point3 Point3::operator/ (const double _a) const
{
    return div(_a);
}

