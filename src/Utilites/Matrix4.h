//
// Created by Suraj Subudhi on 24-Jun-17.
//

#ifndef LEARNRAYTRACER_MATRIX4_H
#define LEARNRAYTRACER_MATRIX4_H


class Matrix4 {
public:

    Matrix4();
    Matrix4(const Matrix4& mat);
    ~Matrix4();

    Matrix4& operator=(const Matrix4& rhs);
    Matrix4& operator*(const Matrix4& mat) const ;
    Matrix4  operator/(const double a);

    //void transpose();


    double m[4][4];
};


#endif //LEARNRAYTRACER_MATRIX4_H
