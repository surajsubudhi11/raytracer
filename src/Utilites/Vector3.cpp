//
// Created by Suraj Subudhi on 24-Jun-17.
//

#include "Vector3.h"
#include "Normal3.h"


Vector3::Vector3(): x(0), y(0), z(0)
{
}

Vector3::Vector3(double _x, double _y, double _z): x(_x), y(_y), z(_z)
{
}

Vector3::Vector3(const Vector3& _vec)
{
    x = _vec.x;
    y = _vec.y;
    z = _vec.z;
}

Vector3 Vector3::add(const Vector3& _vec) const
{
    return Vector3(x + _vec.x, y + _vec.y, z + _vec.z);
}

Vector3 Vector3::sub(const Vector3& _vec) const
{
    return Vector3(x - _vec.x, y - _vec.y, z - _vec.z);
}

Vector3 Vector3::mult(const double _a) const
{
    return Vector3(x * _a , y * _a, z * _a);
}

Vector3 Vector3::div(const double _a) const
{
    assert(_a != 0.0);
    return Vector3(x / _a , y / _a, z / _a);
}

double Vector3::squaredLength() const
{
    return (x*x + y*y + z*z);
}

double Vector3::length() const
{
    return sqrt(x*x + y*y + z*z);
}

void Vector3::normalized()
{
    double length = sqrt(x*x + y*y + z*z);
    x = (x / length);
    y = (y / length);
    z = (z / length);
}



Vector3& Vector3::hat()
{
    double length = sqrt(x * x + y * y + z * z);
    x = (x / length);
    y = (y / length);
    z = (z / length);
    return (*this);
}

double Vector3::dot(const Vector3& _vec) const
{
    return (x * _vec.x + y * _vec.y + z * _vec.z);
}

double Vector3::dot(const Normal& _vec) const
{
    return (x * _vec.x + y * _vec.y + z * _vec.z);
}

Vector3 Vector3::cross(const Vector3& _vec) const
{
    double i = y*_vec.z - z*_vec.y;
    double j = z*_vec.x - x*_vec.z;
    double k = x*_vec.y - y*_vec.x;
    return Vector3(i, j, k);
}

Vector3 Vector3::negate()
{
    return mult(-1);
}

Vector3& Vector3::operator= (const Vector3& rhs)
{
    if (this == &rhs)
        return (*this);

    x = rhs.x; y = rhs.y; z = rhs.z;

    return (*this);

}
//
//Vector3& Vector3::operator= (const Normal& rhs)
//{
//
//    x = rhs.x; y = rhs.y; z = rhs.z;
//
//    return (*this);
//
//}
//
//Vector3& Vector3::operator= (const Point3& rhs)
//{
//
//    x = rhs.x; y = rhs.y; z = rhs.z;
//
//    return (*this);
//
//}


Vector3 Vector3::operator+ (const Vector3& v) const
{
    return add(v);
}


Vector3 Vector3::operator- (const Vector3& v) const
{
    return sub(v);
}

double Vector3::operator* (const Vector3& v) const
{
    return dot(v);
}

Vector3 Vector3::operator* (double a) const
{
    return mult(a);
}

Vector3 Vector3::operator/ (double a) const
{
    return div(a);
}

Vector3::~Vector3()
{
}