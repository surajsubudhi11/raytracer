//
// Created by suraj on 6/26/17.
//

#include "Point2.h"

Point2::Point2() : x(0), y(0)
{

}

Point2::Point2(const double _a) : x(_a), y(_a)
{

}

Point2::Point2(const double _x, const double _y) : x(_x), y(_y)
{

}

Point2::Point2(const Point2& _p)
{
    x = _p.x;
    y = _p.y;
}

Point2::~Point2(){}

Point2 Point2::mult(const double _a) const
{
    return Point2(_a * x, _a * y);
}

Point2 Point2::div(const double _a) const
{
    return Point2(_a / x, _a / y);

}

double Point2::squaredDistance(const Point2& p) const
{
    return x*x + y*y;
}

double Point2::distance(const Point2& p) const
{
    return sqrt(squaredDistance(p));
}

Point2& Point2::operator= (const Point2& rhs)
{
    if (this == &rhs)
        return (*this);

    x = rhs.x;
    y = rhs.y;
    return (*this);
}

Point2 Point2::operator* (const double _a) const
{
    return mult(_a);
}

Point2 Point2::operator/ (const double _a) const
{
    return div(_a);
}