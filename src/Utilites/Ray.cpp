//
// Created by Suraj Subudhi on 24-Jun-17.
//

#include "Ray.h"

Ray::Ray() : origin(Point3()), direction(Vector3(1, 0, 0))
{
    direction.normalized();
}

Ray::Ray(const Point3& _orig, const Vector3& _dir) : origin(_orig), direction(_dir)
{
    //direction.normalized();
    direction = direction.hat();
}

Ray::Ray(const Ray& ray)
{
    origin = ray.origin;
    direction = ray.direction;
    direction = direction.hat();
}

Ray::~Ray(){}

Ray& Ray::operator= (const Ray& rhs)
{
    if (this == &rhs)
        return (*this);

    origin = rhs.origin;
    direction = rhs.direction;

    return (*this);

}