//
// Created by Suraj Subudhi on 24-Jun-17.
//

#include "Normal3.h"

Normal::Normal() : x(0), y(0), z(0)
{

}

Normal::Normal(const Normal& _norm)
{
    x = _norm.x;
    y = _norm.y;
    z = _norm.z;
}

Normal::Normal(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
{

}

Normal Normal::add(const Normal& _norm) const
{
    return Normal(x + _norm.x, y + _norm.y, z + _norm.z);
}

Normal Normal::mult(const double _a) const
{
    return Normal(x * _a, y * _a, z * _a);
}

Normal& Normal::hat() {
    double length = sqrt(x * x + y * y + z * z);
    x = (x / length);
    y = (y / length);
    z = (z / length);
    return (*this);
}

double Normal::dot(const Vector3& _vec) const
{
    return (x*_vec.x + y*_vec.y + z*_vec.z);
}

Vector3 Normal::add(const Vector3& _vec) const
{
    return Vector3(x + _vec.x, y + _vec.y, z + _vec.z);
}

Normal& Normal::operator= (const Normal& rhs)
{
    if (this == &rhs)
        return (*this);

    x = rhs.x; y = rhs.y; z = rhs.z;

    return (*this);
}

Normal& Normal::operator= (const Vector3& rhs)
{
    x = rhs.x; y = rhs.y; z = rhs.z;

    return (*this);
}


Vector3 Normal::operator+ (const Vector3& v)
{
    return add(v);
}

Normal Normal::operator+ (const Normal& n) const
{
    return add(n);
}

double  Normal::operator* (const Vector3& v) const
{
    return dot(v);
}

Normal Normal::operator* (double a) const
{
    return mult(a);
}

void Normal::negate()
{
    x = -1 * x;
    y = -1 * y;
    z = -1 * z;
}