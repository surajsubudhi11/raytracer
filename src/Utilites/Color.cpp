//
// Created by Suraj Subudhi on 24-Jun-17.
//

#include "Color.h"
#include "Maths.h"


Color::Color() : r(0.0), g(0.0), b(0.0)
{

}

Color::Color(float c) : r(c), g(c), b(c)
{

}

Color::Color(float _r, float _g, float _b) : r(_r), g(_g), b(_b)
{

}

Color::Color(const Color& col)
{
    r = col.r;
    g = col.g;
    b = col.b;
}

Color& Color::operator=(const Color &rhs)
{
    if (this == &rhs)
        return (*this);

    r = rhs.r;
    g = rhs.g;
    b = rhs.b;

    return (*this);
}

Color::~Color(){}


Color Color::max_to_one() const
{
    float max_value = (float)max(r, max(g, b));
    if(max_value > 1.0)
    {
        return Color(r/ max_value, g/max_value, b/max_value);
    } else
        return Color(r, g, b);
}

Color Color::clampColor(const Color& col) const
{
    Color tempCol(col);

    if(col.r > 1.0 || col.g > 1.0 || col.b > 1.0) {
        tempCol.r = 1.0;
        tempCol.g = 0.0;
        tempCol.b = 0.0;
    }

    return tempCol;
}