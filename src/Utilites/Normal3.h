//
// Created by Suraj Subudhi on 24-Jun-17.
//

#ifndef LEARNRAYTRACER_NORMAL3_H
#define LEARNRAYTRACER_NORMAL3_H

#include "Vector3.h"

class Normal {

public:
    Normal();
    Normal(const Normal& _norm);
    Normal(double _x, double _y, double _z);

    Normal add(const Normal& _norm) const;
    Normal mult(const double _a) const;
    Normal& hat();

    double dot(const Vector3& _vec) const;
    Vector3 add(const Vector3& _vec) const;
    void negate();

    Normal& operator= (const Normal& rhs);
    Normal& operator= (const Vector3& rhs);

    Vector3 operator+ (const Vector3& v);
    Normal operator+ (const Normal& n) const;
    double  operator* (const Vector3& v) const;
    Normal operator* (double a) const;

    double x, y, z;
};


#endif //LEARNRAYTRACER_NORMAL3_H
