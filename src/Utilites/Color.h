//
// Created by Suraj Subudhi on 24-Jun-17.
//

#ifndef LEARNRAYTRACER_COLOR_H
#define LEARNRAYTRACER_COLOR_H


class Color {

public:
    Color();
    Color(float c);
    Color(float _r, float _g, float _b);
    Color(const Color& col);
    ~Color();

    inline Color operator+ (const Color& c) const {
        return (Color(r + c.r, g + c.g, b + c.b));
    }

    inline Color& operator+= (const Color& c){
        r += c.r; g += c.g; b += c.b;
        return (*this);
    }

    inline Color operator* (const float a) const {
        return (Color (r * a, g * a, b * a));
    }

    inline Color operator* (const float a) {
        return (Color (r * a, g * a, b * a));
    }

    inline Color operator* (const int a) {
        return (Color (r * a, g * a, b * a));
    }

    inline Color operator* (const Color& col) {
        return (Color (r * col.r, g * col.g, b * col.b));
    }

    inline bool operator== (const Color& c) const {
        return (r == c.r && g == c.g && b == c.b);
    }

    Color& operator= (const Color& rhs);

    Color max_to_one() const;
    Color clampColor(const Color& col) const;

    float r, g, b;
};


#endif //LEARNRAYTRACER_COLOR_H
