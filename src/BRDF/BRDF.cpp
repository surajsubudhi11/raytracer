//
// Created by Suraj Subudhi on 7/4/17.
//

#include "BRDF.h"


BRDF::BRDF() : samples_ptr(NULL)
{

}

BRDF::~BRDF()
{
    if(samples_ptr)
        delete samples_ptr;
    samples_ptr = NULL;
}

BRDF::BRDF(const BRDF &brdf)
{
    samples_ptr = brdf.samples_ptr;
}

BRDF& BRDF::operator=(const BRDF &brdf)
{
    if(this == &brdf)
        return *this;

    if(brdf.samples_ptr)
        samples_ptr = brdf.samples_ptr;
    else
        samples_ptr = NULL;

    return *this;

}