//
// Created by Suraj Subudhi on 7/4/17.
//

#ifndef LEARNRAYTRACER_BRDF_H
#define LEARNRAYTRACER_BRDF_H


#include "../Utilites/Color.h"
#include "../Utilites/ShadeData.h"
#include "../Samplers/Samples.h"

class BRDF {

public:
    BRDF();
    virtual ~BRDF();

    BRDF(const BRDF& brdf);
    BRDF& operator=(const BRDF& brdf);

    inline void SetSampler(Samples* samp){samples_ptr = samp;}

    virtual Color brdf_func(const ShadeData& sd, const Vector3& w0, Vector3& wi) const = 0;
    virtual Color sample_brdf_func(const ShadeData& sd, const Vector3& w0, Vector3& wi) const = 0;
    virtual Color reflectance(const ShadeData& sd, const Vector3& w0) const = 0;

protected:
    Samples* samples_ptr;
};


#endif //LEARNRAYTRACER_BRDF_H
