//
// Created by Suraj Subudhi on 7/4/17.
//

#include "Lambertian.h"
#include "../Utilites/Maths.h"

Lambertian::Lambertian() : BRDF(), kd(1.0), cd(Color())
{

}

Lambertian::~Lambertian()
{

}

Lambertian::Lambertian(const Lambertian& lambert) : BRDF(), kd(lambert.kd), cd(lambert.cd)
{

}

Lambertian& Lambertian::operator=(const Lambertian& rhs)
{
    if(this == &rhs)
        return *this;

    BRDF::operator=(rhs);

    kd = rhs.kd;
    cd = rhs.cd;

    return *this;

}

Color Lambertian::brdf_func(const ShadeData& sd, const Vector3& wo, Vector3& wi) const
{
    return (cd * (float)(kd * invPI));
}


Color Lambertian::sample_brdf_func(const ShadeData& sd, const Vector3& wo, Vector3& wi) const
{
    // TODO Adding a Sample brdf Function
    return Color(0.0);
}

Color Lambertian::reflectance(const ShadeData& sd, const Vector3& wo) const
{
    return (cd * kd);
}