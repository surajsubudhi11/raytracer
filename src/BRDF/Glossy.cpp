//
// Created by Suraj Subudhi on 7/8/17.
//

#include "Glossy.h"

Glossy::Glossy() : BRDF(), ks(1.0), cs(Color(0.0)), exp(1)
{

}

Glossy::~Glossy(){}

Glossy::Glossy(const Glossy& gl) : BRDF(), ks(gl.ks), cs(gl.cs), exp(gl.exp)
{

}

Glossy& Glossy::operator=(const Glossy& rhs)
{
    if(this == &rhs)
        return *this;

    BRDF::operator=(rhs);

    ks = rhs.ks;
    cs = rhs.cs;
    exp = rhs.exp;

    return *this;
}

Color Glossy::brdf_func(const ShadeData& sd, const Vector3& wo, Vector3& wi) const
{
    Color L;
    double ndoti = sd.normal.dot(wi);
    Normal tempNorm = (sd.normal.mult(ndoti * 2));
    Vector3 r((wi * -1 ) + Vector3(tempNorm.x, tempNorm.y, tempNorm.z));

    double rdotwo = r.dot(wo);

    if(rdotwo > 0.0){
        L = (cs * ks) * (float)pow(rdotwo, exp);
    }

    return L;
}

Color Glossy::sample_brdf_func(const ShadeData& sd, const Vector3& wo, Vector3& wi) const
{
    return Color(0.0);
}

Color Glossy::reflectance(const ShadeData& sd, const Vector3& wo) const
{
    return Color(0.0);
}