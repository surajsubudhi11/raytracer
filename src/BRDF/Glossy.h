//
// Created by Suraj Subudhi on 7/8/17.
//

#ifndef LEARNRAYTRACER_GLOSSY_H
#define LEARNRAYTRACER_GLOSSY_H


#include "BRDF.h"

class Glossy : public BRDF
{
public:
    Glossy();
    ~Glossy();

    Glossy(const Glossy& gl);
    Glossy& operator=(const Glossy& rhs);

    Color brdf_func(const ShadeData& sd, const Vector3& wo, Vector3& wi) const  override;
    Color sample_brdf_func(const ShadeData& sd, const Vector3& wo, Vector3& wi) const  override;
    Color reflectance(const ShadeData& sd, const Vector3& wo) const  override;

    inline void set_ks(const float& k) {ks = k;}
    inline void set_cs(const Color& col) {cs = col;}
    inline void set_exp(const float& e) {exp = e;}

    inline float get_ks() {return ks;}
    inline float get_exp() {return exp;}
    inline Color get_cs() {return cs;}

private:
    float ks;
    Color cs;
    float exp;

};


#endif //LEARNRAYTRACER_GLOSSY_H
