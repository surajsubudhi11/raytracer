//
// Created by suraj on 7/4/17.
//

#ifndef LEARNRAYTRACER_LAMBERTIAN_H
#define LEARNRAYTRACER_LAMBERTIAN_H


#include "BRDF.h"

class Lambertian : public BRDF
{
public:
    Lambertian();
    ~Lambertian();

    Lambertian(const Lambertian& lambert);
    Lambertian& operator=(const Lambertian& rhs);

    Color brdf_func(const ShadeData& sd, const Vector3& wo, Vector3& wi) const  override;
    Color sample_brdf_func(const ShadeData& sd, const Vector3& wo, Vector3& wi) const  override;
    Color reflectance(const ShadeData& sd, const Vector3& wo) const  override;

    inline void set_kd(const float& k) {kd = k;}
    inline void set_cd(const Color& col) {cd = col;}

    inline float get_kd() {return kd;}
    inline Color get_cd() {return cd;}

private:
    float kd;
    Color cd;
};


#endif //LEARNRAYTRACER_LAMBERTIAN_H
