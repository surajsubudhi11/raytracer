//
// Created by Suraj Subudhi on 6/26/17.
//

#include "../World/World.h"
#include "../Objects/Sphere.h"
#include "../Objects/Plane.h"
#include "../Lights/PointLight.h"
#include "../Materials/Matte.h"
#include "../Camera/Orthographic.h"
#include "../Materials/Phong.h"
#include "../Lights/DirectionalLight.h"

void World::build()
{
    background_color = Color(1.0, 0.0, 0.0);

    AmbientLight* ambientLight = new AmbientLight;
    ambientLight->set_ls(4.0);
    ambientLight->set_color(Color(0.0, 0.0, 1.0));
    SetAmbientLightPtr(ambientLight);


    Orthographic* orthoCam_ptr = new Orthographic;
    orthoCam_ptr->SetEye(Point3(0, 0, 120));
    orthoCam_ptr->SetLookAt(Point3(0, 0, 0));
    //pinHole_ptr->SetDistance(10);
    orthoCam_ptr->compute_uvw();
    SetCamera(orthoCam_ptr);

    PointLight* pointLight = new PointLight;
    pointLight->set_location(Point3(740, 0, 510));
    pointLight->set_ls(3.0);
    addLight(pointLight);

//    DirectionalLight* directionalLight = new DirectionalLight;
//    directionalLight->set_direction(Vector3(5, 0, 1));
//    directionalLight->set_ls(1.0);
//    directionalLight->set_color(Color(0.0, 1.0, 0.0));
//    addLight(directionalLight);

//    PointLight* pointLight1 = new PointLight;
//    pointLight1->set_location(Point3(-50, 20, 140));
//    pointLight1->set_ls(1.0);
//    addLight(pointLight1);

    Phong* phong_sphere_ptr = new Phong;
//    Matte* matte_sphere_ptr = new Matte;
    phong_sphere_ptr->set_ka(0.05);
    phong_sphere_ptr->set_kd(0.6);
    phong_sphere_ptr->set_ks(1.0);
    phong_sphere_ptr->set_cd(Color(1, 1, 0));
    phong_sphere_ptr->set_cs(Color(1, 1, 1), 3.5);




    Sphere* sphere_ptr = new Sphere(Point3(0, 0, 50), 60);
    sphere_ptr->set_material(phong_sphere_ptr);
    sphere_ptr->set_color(Color(1.0, 0.0, 1.0));
    addObject(sphere_ptr);

    Phong* phong_sphere_ptr1 = new Phong;
    phong_sphere_ptr1->set_ka(0.05);
    phong_sphere_ptr1->set_kd(0.6);
    phong_sphere_ptr1->set_ks(1.0);
    phong_sphere_ptr1->set_cd(Color(1, 0, 1));
    phong_sphere_ptr1->set_cs(Color(1, 1, 1), 10.5);

    Sphere* sphere_ptr1 = new Sphere(Point3(140, 0, 250), 40);
    sphere_ptr1->set_material(phong_sphere_ptr1);
    sphere_ptr1->set_color(Color(1.0, 0.2, 0.6));
    addObject(sphere_ptr1);

    Phong* matte_plane_ptr = new Phong;
    matte_plane_ptr->set_ka(0.05);
    matte_plane_ptr->set_kd(0.6);
    matte_plane_ptr->set_cd(Color(0, 1, 1));
    matte_plane_ptr->set_cs(Color(0.2, 0.5, 1), 3.5);


    Plane* plane_ptr = new Plane(Point3(0, 0, -10), Normal(0, 0, 1));
    plane_ptr->set_material(matte_plane_ptr);
    addObject(plane_ptr);
}