//
// Created by Suraj Subudhi on 25-Jun-17.
//

#ifndef LEARNRAYTRACER_VIEWPLANE_H
#define LEARNRAYTRACER_VIEWPLANE_H


#include "../Samplers/Samples.h"

class ViewPlane {

public:
    int width;
    int height;
    float pixel_size;

    //TODO Create Get and Setter functions and make member variables private.

    ViewPlane();
    ViewPlane(const ViewPlane& plane);
    ~ViewPlane();

    ViewPlane& operator= (const ViewPlane& rhs);

    void set_sampler(Samples* sp);
    void set_samples(const int n);

    inline int GetNumOfSamples(){ return num_of_samples;}
    inline Samples* GetSamplePtr(){ return sampler_ptr;}

private:
    Samples* sampler_ptr;
    int num_of_samples;
};


#endif //LEARNRAYTRACER_VIEWPLANE_H
