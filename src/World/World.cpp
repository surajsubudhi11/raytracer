//
// Created by Suraj Subudhi on 25-Jun-17.
//

#include "World.h"
#include "./../Utilites/ShadeData.h"
#include "../Lights/AmbientLight.h"

#include <iostream>
#include <stdio.h>

bool WriteTGA(char *file, int width, int height, unsigned char *outImage);

World::World() : viewPlane(ViewPlane()),
                 background_color(Color(1, 0, 0)),
                 ambient_light_ptr(new AmbientLight()),
                 local_hit_point()
                 //normal(), local_hit_point()
{
    primaryBuffer = new Color[viewPlane.width * viewPlane.height];
}

World::World(int w, int h) : viewPlane(ViewPlane()),
                             background_color(Color(1, 0, 0)),
                             ambient_light_ptr(new AmbientLight()),
                             local_hit_point()
                             //normal(), local_hit_point()
{
    viewPlane.width = w;
    viewPlane.height = h;
    primaryBuffer = new Color[viewPlane.width * viewPlane.height];
}

World::~World()
{
    Release();
}

void World::addObject(Objects* obj_ptr)
{
    objects.push_back(obj_ptr);
}

void World::addLight(Light *light_ptr)
{
    lights.push_back(light_ptr);
}

bool World::Render()
{
    if(primaryBuffer == NULL)
        return false;

    Ray primaryRay;
    int pixelIndex = 0;
    double zw = 100.0;

    Point2 sp;
    Point2 pp;

    primaryRay.direction = Vector3(0, 0, -1);

    for(int height_inc = 0; height_inc < viewPlane.height; height_inc++)
    {
        for(int width_inc = 0; width_inc < viewPlane.width; width_inc++)
        {
            Color pixelColor;
            for (int i = 0; i < viewPlane.GetNumOfSamples(); i++) {
                sp = viewPlane.GetSamplePtr()->sample_unit_square();

                pp.x = viewPlane.pixel_size * (width_inc - 0.5 * (viewPlane.width) + sp.x);
                pp.y = viewPlane.pixel_size * (height_inc - 0.5 * (viewPlane.height) + sp.y);
                primaryRay.origin = Point3(pp.x, pp.y, zw);
                pixelColor += Trace(primaryRay);
            }
            pixelColor =  pixelColor * (1 / viewPlane.GetNumOfSamples());
            primaryBuffer[pixelIndex++] = pixelColor;

        }
    }

    return true;
}

void World::SetCamera(Camera* _camera_ptr)
{
    camera_ptr = _camera_ptr;
}


bool World::SaveImage(char* filename)
{
    return WriteTGA(filename, 640, 960,(unsigned char*)primaryBuffer);
}

void World::Release()
{
    if(primaryBuffer != NULL)
    {
        delete[] primaryBuffer;
        primaryBuffer = NULL;
    }

    unsigned long num_objects = objects.size();
    for (unsigned int j = 0; j < num_objects; j++) {
        //delete objects[j];
        objects[j] = NULL;
    }
    objects.erase (objects.begin(), objects.end());

    unsigned long num_lights = lights.size();
    for (unsigned int j = 0; j < num_lights; j++) {
        //delete lights[j];
        lights[j] = NULL;
    }
    lights.erase (lights.begin(), lights.end());
}

Color World::Trace(Ray &primaryRay)
{
    ShadeData sr(*this);
    double closestDistance = 20000.0f;
    int closestObj = -1;

    for(unsigned int i = 0; i < objects.size(); i++)
    {
        if(objects[i] == NULL)
            continue;

        double dist = 0;
        if(objects[i]->hit(primaryRay, dist, sr))
        {
            if(dist < closestDistance)
            {
                closestDistance = dist;
                closestObj = i;
                sr.hit_an_object = true;
                sr.color = objects[closestObj]->GetColor();
            }
        }
    }

    if(closestObj != -1)
    {
        return objects[closestObj]->GetColor();
    }

    return background_color;
}


Color World::RayCastTrace(const Ray& primaryRay, const float depth)
{
    ShadeData sd(hit_objects(primaryRay));

    if(sd.hit_an_object) {
        sd.ray = primaryRay;
        return (sd.materials_ptr->RaycastShade(sd));
    } else
        return background_color;

}



ShadeData World::hit_objects(const Ray& ray){
    ShadeData	sr(*this);
    double   	t;
    Normal      normal;
    float		tmin 			= 20000.0f;
    int 		num_objects 	= objects.size();

    for (int j = 0; j < num_objects; j++) {
        if (objects[j]->hit(ray, t, sr) && (t < tmin)) {
            sr.hit_an_object = true;
            tmin = (float) t;
            sr.materials_ptr = objects[j]->get_material_ptr();
            sr.hit_point = ray.origin + (ray.direction.mult(t));
            normal = sr.normal;
            //sr.color			= objects[j]->GetColor();
            local_hit_point = sr.local_hit_point;
        }
    }

    if(sr.hit_an_object)
    {
        sr.normal = normal;
        sr.local_hit_point = local_hit_point;
    }


    return (sr);
}


bool WriteTGA(char *file, int width, int height, unsigned char *outImage)
{
    unsigned char tgaHeader[12] = {0, 0, 2,
                                   0, 0, 0,
                                   0, 0, 0,
                                   0, 0, 0};

    unsigned char header[6];

    FILE *pFile = fopen(file, "wb");
    if(pFile == NULL)
        return false;

    int colorMode = 3;
    unsigned char bits = 24;

    header[0] = width % 256;
    header[1] = width / 256;

    header[2] = height % 256;
    header[3] = height / 256;
    // Component bits (RGB).
    header[4] = bits;
    header[5] = 0;

    fwrite(tgaHeader, sizeof(tgaHeader), 1, pFile);
    fwrite(header, sizeof(header), 1, pFile);


    unsigned char tempColors = 0;
    // Now switch image from RGB to BGR for the TGA file.
    for(int i = 0; i < width * height * colorMode; i += colorMode)
    {
        tempColors = outImage[i];
        outImage[i] = outImage[i + 2];
        outImage[i + 2] = tempColors;
    }

    fwrite(outImage, width * height * colorMode, 1, pFile);
    // close the file.
    fclose(pFile);
    return true;

}

void World::Savebmp(const char *filename){//, int w, int h, int dpi){//, RGBType *data){
    FILE *f;
    int w = viewPlane.width;
    int h = viewPlane.height;
    int dpi = 72;
    int k = w * h;
    int s = 4 * k;
    int filesize = 54 + s;

    double factor = 39.375;
    int m = static_cast<int>(factor);

    int ppm = dpi * m;

    unsigned char bmpfileheader[14] = {'B', 'M',  0,0,0,0, 0,0,0,0, 54,0,0,0};
    unsigned char bmpinfoheader[40] = { 40,0,0,0, 0,0,0,0, 0,0,0,0,  1,0,24,0};

    bmpfileheader[ 2] = (unsigned char )(filesize);
    bmpfileheader[ 3] = (unsigned char )(filesize >>  8);
    bmpfileheader[ 4] = (unsigned char )(filesize >> 16);
    bmpfileheader[ 5] = (unsigned char )(filesize >> 24);

    bmpinfoheader[ 4]  = (unsigned char )(w);
    bmpinfoheader[ 5]  = (unsigned char )(w >>  8);
    bmpinfoheader[ 6]  = (unsigned char )(w >> 16);
    bmpinfoheader[ 7]  = (unsigned char )(w >> 24);

    bmpinfoheader[ 8]  = (unsigned char )(h);
    bmpinfoheader[ 9]  = (unsigned char )(h >>  8);
    bmpinfoheader[10]  = (unsigned char )(h >> 16);
    bmpinfoheader[11]  = (unsigned char )(h >> 24);

    bmpinfoheader[21]  = (unsigned char )(s);
    bmpinfoheader[22]  = (unsigned char )(s >>  8);
    bmpinfoheader[23]  = (unsigned char )(s >> 16);
    bmpinfoheader[24]  = (unsigned char )(s >> 24);

    bmpinfoheader[25]  = (unsigned char )(ppm);
    bmpinfoheader[26]  = (unsigned char )(ppm >>  8);
    bmpinfoheader[27]  = (unsigned char )(ppm >> 16);
    bmpinfoheader[28]  = (unsigned char )(ppm >> 24);

    bmpinfoheader[29]  = (unsigned char )(ppm);
    bmpinfoheader[30]  = (unsigned char )(ppm >>  8);
    bmpinfoheader[31]  = (unsigned char )(ppm >> 16);
    bmpinfoheader[32]  = (unsigned char )(ppm >> 24);

    f = fopen(filename, "wb");

    fwrite(bmpfileheader, 1, 14, f);
    fwrite(bmpinfoheader, 1, 40, f);

    for(int i = 0; i < k; i++){
        Color rgb;
        //std::cout << primaryBuffer[i].r << "g : " << primaryBuffer[i].g << std::endl;
        rgb = primaryBuffer[i];

        double red   = (primaryBuffer[i].r) * 255;
        double green = (primaryBuffer[i].g) * 255;
        double blue  = (primaryBuffer[i].b) * 255;

        unsigned char color[3] = {(unsigned char)floor(blue), (unsigned char)floor(green), (unsigned char)floor(red)};
        fwrite(color, 1, 3, f);
    }

    fclose(f);
}