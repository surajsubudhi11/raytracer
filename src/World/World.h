//
// Created by Suraj Subudhi on 25-Jun-17.
//

#ifndef LEARNRAYTRACER_WORLD_H
#define LEARNRAYTRACER_WORLD_H

#include <vector>

#include "ViewPlane.h"
#include "./../Objects/Objects.h"
#include "../Camera/Camera.h"
#include "../Lights/Light.h"
#include "../Lights/AmbientLight.h"


class World {

public:
    World();
    World(int w, int h);
    ~World();

    void build();

    void addObject(Objects* obj_ptr);
    void addLight(Light* light_ptr);
    bool Render();

    bool SaveImage(char* filename);
    void Savebmp(const char *filename);//, int w, int h, int dpi);
    void Release();
    void SetCamera(Camera* _camera_ptr);

    ShadeData hit_objects(const Ray& ray);

    inline Color* GetColorBuffer(){ return primaryBuffer;}
    inline void SetColorAt(const int index, Color _col){primaryBuffer[index] = _col;}

    inline ViewPlane GetViewPlane(){ return viewPlane;}

    inline Light* GetAmbientLightPtr(){ return ambient_light_ptr;}
    inline void SetAmbientLightPtr(AmbientLight* amb){ ambient_light_ptr = amb;}

    inline int NumOfLights(){ return lights.size();}
    inline int NumOfObjects(){ return objects.size();}

    inline Light* GetLightAtIndex(const int _index){ return lights[_index];}
    inline Objects* GetObjectAtIndex(const int _index){ return objects[_index];}

    Color Trace(Ray &primaryRay);
    Color RayCastTrace(const Ray& primaryRay, const float depth);

public:
    Camera* camera_ptr;
private:
    //Normal normal;
    Point3 local_hit_point;

    Light* ambient_light_ptr;

    ViewPlane viewPlane;
    Color background_color;
    std::vector<Objects* > objects;

    Color* primaryBuffer;
    std::vector<Light* > lights;

};


#endif //LEARNRAYTRACER_WORLD_H
