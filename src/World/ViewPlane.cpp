//
// Created by Suraj Subudhi on 25-Jun-17.
//

#include "ViewPlane.h"
#include "../Samplers/Regular.h"
#include "../Samplers/JitteredSample.h"


ViewPlane::ViewPlane() : width(400), height(600), pixel_size(1.0), num_of_samples(1)
{
    sampler_ptr = new Regular(1);
}

ViewPlane::ViewPlane(const ViewPlane& plane)
{
    width = plane.width;
    height = plane.height;
    pixel_size = plane.pixel_size;
    sampler_ptr = plane.sampler_ptr;
    num_of_samples = plane.num_of_samples;
}

ViewPlane::~ViewPlane()
{
    //delete sampler_ptr;
    sampler_ptr = NULL;
}

ViewPlane& ViewPlane::operator= (const ViewPlane& rhs)
{
    if (this == &rhs)
        return (*this);

    width = rhs.width;
    height = rhs.height;
    pixel_size = rhs.pixel_size;
    sampler_ptr = rhs.sampler_ptr;
    num_of_samples = rhs.num_of_samples;

    return (*this);
}

void ViewPlane::set_sampler(Samples* sp)
{
    if(sampler_ptr){
        delete sampler_ptr;
        sampler_ptr = NULL;
    }

    num_of_samples = sp->GetNumSamples();
    sampler_ptr = sp;

}

void ViewPlane::set_samples(const int n)
{
    num_of_samples = n;

    if(sampler_ptr){
        delete sampler_ptr;
        sampler_ptr = NULL;
    }

    if(num_of_samples > 1){
        // TODO We will be using Multi Jittered Sampling for greater than 1.
        sampler_ptr = new JitteredSample(num_of_samples);
    } else{
        sampler_ptr = new Regular(num_of_samples);
    }
}