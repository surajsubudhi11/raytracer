//
// Created by Suraj Subudhi on 25-Jun-17.
//

#ifndef LEARNRAYTRACER_SPHERE_H
#define LEARNRAYTRACER_SPHERE_H


#include "Objects.h"

class Sphere : public Objects
{
public:
    Sphere();
    ~Sphere();

    Sphere(const Point3& cen, double rad);
    Sphere(const Sphere& s);

    void SetCenter(const Point3& cen);
    void SetCenter(double x, double y, double z);
    void SetRadius(double rad);

    Point3 GetCenter();
    double GetRadius();

    Sphere& operator=(const Sphere& rhs);

    bool hit(const Ray& ray, double& tmin, ShadeData& sr) const override;
    bool shadow_hit(const Ray& ray, double& tmin) const override;

private:
    double radius;
    Point3 center;

    static const float kEpsilon;
};


#endif //LEARNRAYTRACER_SPHERE_H
