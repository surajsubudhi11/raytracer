//
// Created by Suraj Subudhi on 24-Jun-17.
//

#include "Objects.h"

Objects::Objects() : color(Color(1.0, 1.0, 0.0)), material_ptr(nullptr)
{

}

Objects::Objects(const Objects& obj): material_ptr(nullptr)
{
    color = obj.color;
}

Objects::~Objects(){
    material_ptr = nullptr;
}