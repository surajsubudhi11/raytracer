//
// Created by Suraj Subudhi on 25-Jun-17.
//

#include "Sphere.h"
#include "../Utilites/Maths.h"


const float Sphere::kEpsilon = 0.0001;

Sphere::Sphere() : Objects(), center(Point3()), radius(1.0)
{

}

Sphere::~Sphere()
{

}

Sphere::Sphere(const Point3& cen, double rad) : Objects(), center(cen), radius(rad)
{

}

Sphere::Sphere(const Sphere& s) : Objects()
{
    center = s.center;
    radius = s.radius;
}

void Sphere::SetCenter(const Point3& cen)
{
    center = cen;
}

void Sphere::SetCenter(double x, double y, double z)
{
    center = Point3(x, y, z);
}

void Sphere::SetRadius(double rad)
{
    radius = rad;
}

Point3 Sphere::GetCenter()
{
    return center;
}

double Sphere::GetRadius()
{
    return radius;
}

Sphere& Sphere::operator=(const Sphere& rhs)
{
    if (this == &rhs)
        return (*this);

    material_ptr = rhs.material_ptr;
    center = rhs.center;
    radius = rhs.radius;

    return (*this);
}

bool Sphere::hit(const Ray& ray, double& tmin, ShadeData& sr) const
{
    // https://en.wikipedia.org/wiki/Line%E2%80%93sphere_intersection

    Vector3 l = ray.direction;
    l = l.hat();
    Point3  o = ray.origin;
    Point3  c = center;
    double  r = radius;

    Vector3 temp = (o.sub(c));
    //temp = temp.hat();

    double temp1 = pow(l.dot(temp), 2);
    double temp2 =  (o.sub(c)).squaredLength();
    double dis = temp1 - temp2 + pow(r, 2);

    if(dis < 0){
        return false;
    } else if(dis == 0){
        tmin = -1 * l.dot(temp);
        sr.local_hit_point = ray.origin + ray.direction.mult(tmin);
        sr.normal = (sr.local_hit_point - center).hat();
        sr.color = color;
        return true;
    } else{
        double t1 = -1 * l.dot(temp) - sqrt(dis);
        double t2 = -1 * l.dot(temp) + sqrt(dis);
        tmin = min(t1, t2);
        sr.local_hit_point = ray.origin + ray.direction.mult(tmin);
        sr.normal = (sr.local_hit_point - center).hat();
        sr.color = color;
        return true;
    }


}


bool Sphere::shadow_hit(const Ray& ray, double& tmin) const
{


    Vector3 r = ray.origin - center;
    double b = ray.direction.dot(r);
    double c = r.squaredLength() - pow(radius, 2);
    double d = b * b - c;

    if (d > 0.) {
        d = sqrt(d);
        double tMin = -b - d;
        double tMax = -b + d;

        if (tMin < 0.0) {
            tMin = tMax;
        }

        if (tMin >= kEpsilon) {
            tmin = tMin;
            return true;
        }
    }

    return false;

}