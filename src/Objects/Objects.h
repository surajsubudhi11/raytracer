//
// Created by Suraj Subudhi on 24-Jun-17.
//

#ifndef OBJECTS_H
#define OBJECTS_H

//class ShadeData;

#include "./../Utilites/Color.h"
#include "./../Utilites/Ray.h"
#include "../Utilites/ShadeData.h"
#include "../Materials/Materials.h"

class Objects {
public:
    Objects();
    Objects(const Objects& obj);
    virtual ~Objects();

    inline Color GetColor()
    {
        return color;
    }

    inline void set_color(const Color& c){
        color = c;
    }

    inline void set_material(Materials* mat)
    {
        material_ptr = mat;
    }

    inline Materials* get_material_ptr()
    {
        return material_ptr;
    }

    virtual bool hit(const Ray& ray, double& tmin, ShadeData& sr) const = 0;
    virtual bool shadow_hit(const Ray& ray, double & tmin) const = 0;


protected:
    Color color;
    Materials* material_ptr;
};


#endif //OBJECTS_H
