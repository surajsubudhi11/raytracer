//
// Created by Suraj Subudhi on 25-Jun-17.
//

#include "Plane.h"

const double Plane::kEpsilon = 0.001;

Plane::Plane() : Objects(), point(Point3()), normal(Normal(0, 1, 0))
{
    set_color(Color(0, 1, 1));
}

Plane::Plane(const Point3& _point, const Normal& _norm) : Objects(), point(_point), normal(_norm)
{
    set_color(Color(0, 1, 1));
}

Plane::Plane(const Plane& plane) : Objects()
{
    point = plane.point;
    normal = plane.normal;
}

Plane::~Plane()
{

}

bool Plane::hit(const Ray& ray, double& tmin, ShadeData& sr) const
{

//    Point3 pO = point;
//    Normal n = normal;
//    n = n.hat();
//    Point3 lO = ray.origin;
//    Vector3 l = ray.direction;
//
//    double d = 0;
//
//    if(l.dot(n) == 0){
//        return false;
//    } else {
//        d = ((pO.sub(lO)).dot(n)) / (l.dot(n));
//        tmin = d;
//        sr.normal = normal;
//        sr.local_hit_point = ray.origin + (ray.direction.mult(d));
//        //return true;
//    }


    double t = (point - ray.origin).dot(normal) / (ray.direction.dot(normal));

    if(t > kEpsilon){
        tmin = t;
        sr.normal = normal;
        sr.local_hit_point = ray.origin + (ray.direction.mult(t));
        return true;
    } else{
        return false;
    }
}


bool Plane::shadow_hit(const Ray &ray, double &tmin) const
{

//    Point3 pO = point;
//    Normal n = normal;
//    n = n.hat();
//    Point3 lO = ray.origin;
//    Vector3 l = ray.direction;
//
//    double d = 0;
//
//    if(l.dot(n) == 0){
//        return false;
//    } else {
//        d = ((pO.sub(lO)).dot(n)) / (l.dot(n));
//        tmin = d;
//        return true;
//    }


    double t = (point - ray.origin).dot(normal) / (ray.direction.dot(normal));

    if(t > kEpsilon){
        tmin = t;
        return true;
    } else{
        return false;
    }
}