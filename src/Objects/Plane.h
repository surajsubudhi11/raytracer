//
// Created by Suraj Subudhi on 25-Jun-17.
//

#ifndef LEARNRAYTRACER_PLANE_H
#define LEARNRAYTRACER_PLANE_H

#include "Objects.h"
#include "./../Utilites/Normal3.h"
#include "./../Utilites/Point3.h"

class Plane : public Objects
{
public:
    Plane();
    Plane(const Point3& _point, const Normal& _norm);
    Plane(const Plane& plane);
    ~Plane();


    bool hit(const Ray& ray, double& tmin, ShadeData& sr) const override;
    bool shadow_hit(const Ray& ray, double& tmin) const override;

    Point3 point;
    Normal normal;
    static const double kEpsilon;
};


#endif //LEARNRAYTRACER_PLANE_H
