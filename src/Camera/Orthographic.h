//
// Created by Suraj Subudhi on 7/6/17.
//

#ifndef LEARNRAYTRACER_ORTHOGRAPHIC_H
#define LEARNRAYTRACER_ORTHOGRAPHIC_H


#include "Camera.h"

class Orthographic : public Camera
{
public:
    Orthographic();
    ~Orthographic();

    Orthographic(const Orthographic& pinHole);

    Orthographic& operator=(const Orthographic& rhs);

    virtual void Render(World& wr) override;

};




#endif //LEARNRAYTRACER_ORTHOGRAPHIC_H
