//
// Created by Suraj Subudhi on 26-Jun-17.
//

#ifndef LEARNRAYTRACER_CAMERA_H
#define LEARNRAYTRACER_CAMERA_H


#include "../Utilites/Point3.h"
class World;

class Camera {

public:
    Camera();
    virtual ~Camera();

    Camera(const Point3& _eye, const Point3& _lookAt, const Vector3& _up = Vector3(0, 1, 0));
    Camera(const Point3& _eye);
    Camera(const Camera& cam);

    Camera& operator=(const Camera& rhs);

    void compute_uvw();
    virtual void Render(World& wr) = 0;

    inline void SetEye(const Point3& _pos){eye = _pos;}
    inline void SetLookAt(const Point3& _lookAt){lookAt = _lookAt;}
    inline void SetUp(const Vector3& _up){up = _up;}

protected:
    Point3 eye;
    Point3 lookAt;
    Vector3 up;

    Vector3 u, v, w;

};


#endif //LEARNRAYTRACER_CAMERA_H
