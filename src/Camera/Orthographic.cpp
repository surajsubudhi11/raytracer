//
// Created by Suraj Subudhi on 26-Jun-17.
//

#include "Orthographic.h"
#include "./../World/World.h"

Orthographic::Orthographic() : Camera()
{

}

Orthographic::~Orthographic(){}

Orthographic::Orthographic(const Orthographic& ortho): Camera(ortho)
{
}


Orthographic& Orthographic::operator=(const Orthographic& rhs)
{
    if(this == &rhs)
        return *this;

    Camera::operator=(rhs);

    return *this;
}



void Orthographic::Render(World& wr)
{
    if(wr.GetColorBuffer() == NULL)
        return ;

    Ray primaryRay;
    int pixelIndex = 0;
    float zw = 210.0; // Distance from Camera.
    ViewPlane vp(wr.GetViewPlane());

    Point2 sp;
    Point2 pp;

    primaryRay.direction = Vector3(0, 0, -1);

    for(int height_inc = 0; height_inc < vp.height; height_inc++)
    {
        for(int width_inc = 0; width_inc < vp.width; width_inc++)
        {
            Color pixelColor;
            for (int i = 0; i < vp.GetNumOfSamples(); i++) {
                sp = vp.GetSamplePtr()->sample_unit_square();

                pp.x = vp.pixel_size * (width_inc  - 0.5 * (vp.width)  + sp.x);
                pp.y = vp.pixel_size * (height_inc - 0.5 * (vp.height) + sp.y);
                primaryRay.origin = Point3(pp.x, pp.y, zw);
                pixelColor += wr.RayCastTrace(primaryRay, zw).max_to_one();
//                pixelColor += wr.Trace(primaryRay).max_to_one();
            }
            pixelColor =  pixelColor * (1 / vp.GetNumOfSamples());
            wr.SetColorAt(pixelIndex++, pixelColor);

        }
    }

    return;
}

