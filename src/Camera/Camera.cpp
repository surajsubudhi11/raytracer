//
// Created by Suraj Subudhi on 26-Jun-17.
//

#include "Camera.h"

Camera::Camera() : eye(Point3(0, 0, 500)),
                   lookAt(Point3(0, 0, 0)),
                   up(Vector3(0, 1, 0))
{
    compute_uvw();
}

Camera::~Camera()
{

}

Camera::Camera(const Point3& _eye, const Point3& _lookAt, const Vector3& _up )
        : eye(_eye), lookAt(_lookAt), up(_up)
{
    compute_uvw();
}

Camera::Camera(const Point3& _eye): eye(_eye), lookAt(Point3()), up(Vector3(0, 1, 0))
{
    compute_uvw();
}

Camera::Camera(const Camera& cam)
{
    eye = cam.eye;
    lookAt = cam.lookAt;
    up = cam.up;
    u = cam.u;
    v = cam.v;
    w = cam.w;

}

Camera& Camera::operator=(const Camera& rhs)
{
    if(this == &rhs)
        return *this;

    eye = rhs.eye;
    lookAt = rhs.lookAt;
    up = rhs.up;
    u = rhs.u;
    v = rhs.v;
    w = rhs.w;

    return *this;
}


void Camera::compute_uvw()
{
	if (eye.x == lookAt.x && eye.z == lookAt.z) {
		u = Vector3(0,0,1);
		v = Vector3(1,0,0);
		w = Vector3(0,1,0);
	}else{
		w = eye - lookAt;
		w.normalized();
		//u = w.cross(up);
		u = up.cross(w);
		u.normalized();
		//v = u.cross(w);
		v = w.cross(u);
	}
    
	/*
	if (eye.x == lookAt.x && eye.z == lookAt.z && eye.y > lookAt.y) { // camera looking vertically down
        u = Vector3(0, 0, 1);
        v = Vector3(1, 0, 0);
        w = Vector3(0, 1, 0);
    }

    if (eye.x == lookAt.x && eye.z == lookAt.z && eye.y < lookAt.y) { // camera looking vertically up
        u = Vector3(1, 0, 0);
        v = Vector3(0, 0, 1);
        w = Vector3(0, -1, 0);
    }
	*/

}
