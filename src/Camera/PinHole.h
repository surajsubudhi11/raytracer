//
// Created by Suraj Subudhi on 26-Jun-17.
//

#ifndef LEARNRAYTRACER_PINHOLE_H
#define LEARNRAYTRACER_PINHOLE_H

#include "./../Utilites/Point2.h"
#include "./../Utilites/Ray.h"
#include "./../World/ViewPlane.h"
#include "Camera.h"


class PinHole : public Camera
{
public:
    PinHole();
    ~PinHole();

    PinHole(const PinHole& pinHole);
    PinHole(const float dis);

    PinHole& operator=(const PinHole& rhs);

    Vector3 RayDirection(const Point2& point) const;

    virtual void Render(World& wr) override;

    inline void SetDistance(float dis){distance = dis;}
    inline float GetDistance(float dis){return distance;}

private:
    float distance;
    //TODO Adding Camera Roll And Zoom Effect
};


#endif //LEARNRAYTRACER_PINHOLE_H
