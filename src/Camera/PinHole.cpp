//
// Created by Suraj Subudhi on 26-Jun-17.
//

#include "PinHole.h"
#include "./../World/World.h"

PinHole::PinHole() : Camera(), distance(500)
{

}

PinHole::~PinHole(){}

PinHole::PinHole(const PinHole& pinHole): Camera(pinHole)
{
    distance = pinHole.distance;
}

PinHole::PinHole(const float dis) : Camera(), distance(dis)
{

}

PinHole& PinHole::operator=(const PinHole& rhs)
{
    if(this == &rhs)
        return *this;

    Camera::operator=(rhs);

    distance = rhs.distance;
    return *this;
}

Vector3 PinHole::RayDirection(const Point2& point) const
{
    Vector3 dir = (u * point.x) + (v * point.y) + (w * (-1 * distance));
    dir.normalized();
    return dir;
}

void PinHole::Render(World& wr)
{
    if(wr.GetColorBuffer() == NULL)
        return ;

    Ray primaryRay;
    int pixelIndex = 0;
    float zw = 100.0;
    ViewPlane vp(wr.GetViewPlane());

    Point2 sp;
    Point2 pp;

    //primaryRay.direction = Vector3(0, 0, -1);
    primaryRay.origin = eye;

    for(int height_inc = 0; height_inc < vp.height; height_inc++)
    {
        for(int width_inc = 0; width_inc < vp.width; width_inc++)
        {
            Color pixelColor;
            for (int i = 0; i < vp.GetNumOfSamples(); i++) {
                sp = vp.GetSamplePtr()->sample_unit_square();

                pp.x = vp.pixel_size * (width_inc  - 0.5 * (vp.width)  + sp.x);
                pp.y = vp.pixel_size * (height_inc - 0.5 * (vp.height) + sp.y);
                primaryRay.direction = RayDirection(pp);
                pixelColor += wr.RayCastTrace(primaryRay, zw);
            }
            pixelColor =  pixelColor * (1 / vp.GetNumOfSamples());
            wr.SetColorAt(pixelIndex++, pixelColor);

        }
    }

    return;
}