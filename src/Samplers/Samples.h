//
// Created by suraj on 6/26/17.
//

#ifndef LEARNRAYTRACER_SAMPLES_H
#define LEARNRAYTRACER_SAMPLES_H

#include <vector>
#include <algorithm>
//#include "./../Utilites/Maths.h"

#include "./../Utilites/Point2.h"


class Samples {
public:

    Samples();
    virtual ~Samples();
    Samples(const int num);
    Samples(const int num, const int numOfSets);
    Samples(const Samples& sam);

    Samples& operator=(const Samples& samp);

    inline int GetNumSamples(){ return num_of_samples;}
    inline int GetNumSets(){ return num_of_sets;}

    inline void SetNumSamples(int num){num_of_samples = num;}
    inline void SetNumSets(int num){num_of_sets = num;}

    virtual void generate_samples() = 0;

    void setup_shuffled_indices();
    void suffle_samples();
    Point2 sample_unit_square();

protected:
    int num_of_samples;
    int num_of_sets;
    std::vector<Point2> samples;
    std::vector<int> shuffled_indices;
    unsigned long count;
    int jump;

};


#endif //LEARNRAYTRACER_SAMPLES_H
