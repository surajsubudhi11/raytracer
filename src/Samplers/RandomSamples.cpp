//
// Created by suraj on 6/26/17.
//

#include "RandomSamples.h"
#include "../Utilites/Maths.h"

RandomSamples::RandomSamples() : Samples()
{

}

RandomSamples::~RandomSamples()
{

}

RandomSamples::RandomSamples(const int num) : Samples(num)
{

}

RandomSamples::RandomSamples(const RandomSamples& rand) : Samples(rand)
{
    generate_samples();
}

RandomSamples& RandomSamples::operator=(const RandomSamples& rhs)
{
    if (this == &rhs)
        return (*this);

    Samples::operator= (rhs);

    return (*this);
}

void RandomSamples::generate_samples()
{
    for (int i = 0; i < num_of_sets; i++) {
        for (int j = 0; j < num_of_samples; j++) {
            samples.push_back(Point2(rand_float(), rand_float()));
        }
    }
}