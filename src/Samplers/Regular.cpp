//
// Created by suraj on 6/26/17.
//

#include "Regular.h"


Regular::Regular() : Samples()
{

}

Regular::~Regular(){}

Regular::Regular(const int num) : Samples(num)
{

}

Regular::Regular(const Regular& u) : Samples(u)
{
    generate_samples();
}

Regular& Regular::operator=(const Regular& rhs)
{
    if (this == &rhs)
        return (*this);

    Samples::operator= (rhs);

    return (*this);
}

void Regular::generate_samples()
{
    int n = (int)sqrt(num_of_samples);

    for (int i = 0; i < num_of_sets; i++) {
        for (int j = 0; j < n; j++) {
            for (int k = 0; k < n; k++) {
                samples.push_back((Point2((k + 0.5) / n, (j + 0.5) / n)));
            }
            
        }
    }
}