//
// Created by suraj on 6/26/17.
//

#include "JitteredSample.h"
#include "../Utilites/Maths.h"

JitteredSample::JitteredSample() : Samples()
{

}

JitteredSample::~JitteredSample(){}

JitteredSample::JitteredSample(const int num) : Samples(num)
{

}

JitteredSample::JitteredSample(const JitteredSample& jitter) : Samples(jitter)
{
    generate_samples();
}

JitteredSample& JitteredSample::operator=(const JitteredSample& rhs)
{
    if (this == &rhs)
        return (*this);

    Samples::operator= (rhs);

    return (*this);
}

void JitteredSample::generate_samples()
{
    int n = (int)sqrt(num_of_samples);

    for (int i = 0; i < num_of_sets; i++) {
        for (int j = 0; j < n; j++) {
            for (int k = 0; k < n; k++) {
                Point2 sp((k + rand_float()) / n, (j + rand_float()) / n);
                samples.push_back(sp);
            }

        }
    }
}