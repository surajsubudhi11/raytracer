//
// Created by suraj on 6/26/17.
//

#include "Samples.h"
#include "../Utilites/Maths.h"

Samples::Samples() : num_of_samples(1), num_of_sets(83), count(0), jump(0)
{
    samples.reserve(num_of_samples * num_of_sets);
    setup_shuffled_indices();
}

Samples::~Samples()
{

}

Samples::Samples(const Samples& sam) : num_of_samples(sam.num_of_samples),
                                       num_of_sets(sam.num_of_sets),
                                       samples(sam.samples),
                                       shuffled_indices(sam.shuffled_indices),
                                       count(sam.count),
                                       jump(sam.jump)
{

}

Samples& Samples::operator=(const Samples& rhs)
{
    if (this == &rhs)
        return (*this);

    num_of_sets 		= rhs.num_of_sets;
    num_of_samples		= rhs.num_of_samples;
    samples				= rhs.samples;
    shuffled_indices	    = rhs.shuffled_indices;
    count				= rhs.count;
    jump				= rhs.jump;

    return (*this);
}

Samples::Samples(const int num)  : num_of_samples(num), num_of_sets(83), count(0), jump(0)
{
    samples.reserve(num_of_samples * num_of_sets);
    setup_shuffled_indices();
}

Samples::Samples(const int num, const int numOfSets)  : num_of_samples(num), num_of_sets(numOfSets), count(0), jump(0)
{
    samples.reserve(num_of_samples * num_of_sets);
    setup_shuffled_indices();
}


void Samples::setup_shuffled_indices()
{
    shuffled_indices.reserve(num_of_samples * num_of_sets);
    std::vector<int> indices;

    for(int i = 0; i < num_of_samples; i++){
        indices.push_back(i);
    }

    for(int i = 0; i < num_of_sets; i++){
        std::random_shuffle(indices.begin(), indices.end());
        for (int j = 0; j < num_of_samples; j++) {
            shuffled_indices.push_back(indices[j]);
        }
    }
}

void Samples::suffle_samples()
{
    for (int p = 0; p < num_of_sets; p++){
        for (int i = 0; i <  num_of_samples - 1; i++) {
            int target = rand_int() % num_of_samples + p * num_of_samples;

            float tempX = (float)samples[i + p * num_of_samples + 1].x;
            float tempY = (float)samples[i + p * num_of_samples + 1].y;

            samples[i + p * num_of_samples + 1].x = samples[target].x;
            samples[i + p * num_of_samples + 1].y = samples[target].y;

            samples[target].x = tempX;
            samples[target].y = tempY;
        }
    }
}

Point2 Samples::sample_unit_square()
{
    return (samples[count++ % (num_of_samples * num_of_sets)]);
}