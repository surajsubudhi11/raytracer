//
// Created by suraj on 6/26/17.
//

#ifndef LEARNRAYTRACER_REGULAR_H
#define LEARNRAYTRACER_REGULAR_H


#include "Samples.h"

class Regular : public Samples
{
public:
    Regular();
    virtual ~Regular();

    Regular(const int num);
    Regular(const Regular& u);

    Regular& operator=(const Regular& rhs);

    virtual void generate_samples() override;
};


#endif //LEARNRAYTRACER_REGULAR_H
