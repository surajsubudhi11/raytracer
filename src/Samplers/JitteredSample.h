//
// Created by suraj on 6/26/17.
//

#ifndef LEARNRAYTRACER_JITTEREDSAMPLE_H
#define LEARNRAYTRACER_JITTEREDSAMPLE_H


#include "Samples.h"

class JitteredSample : public Samples
{
public:
    JitteredSample();
    virtual ~JitteredSample();

    JitteredSample(const int num);
    JitteredSample(const JitteredSample& jitter);

    JitteredSample& operator=(const JitteredSample& rhs);
private:
    virtual void generate_samples() override;
};


#endif //LEARNRAYTRACER_JITTEREDSAMPLE_H
