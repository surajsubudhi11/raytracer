//
// Created by suraj on 6/26/17.
//

#ifndef LEARNRAYTRACER_RANDOMSAMPLES_H
#define LEARNRAYTRACER_RANDOMSAMPLES_H


#include "Samples.h"

class RandomSamples : public Samples
{
public:
    RandomSamples();
    virtual ~RandomSamples();

    RandomSamples(const int num);
    RandomSamples(const RandomSamples& rand);

    RandomSamples& operator=(const RandomSamples& rhs);

private:
    virtual void generate_samples() override;
};


#endif //LEARNRAYTRACER_RANDOMSAMPLES_H
