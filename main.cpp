#include <iostream>
#include <stdio.h>

#include "./src/World/World.h"
using namespace std ;

#define WIDTH 768
#define HEIGHT 432

int main () {

    cout << "Raytracing..." << endl;
    World world(WIDTH, HEIGHT);
    //world.GetViewPlane().set_samples(16);
    world.build();
    world.camera_ptr->Render(world);
    //world.Render();

    world.Savebmp("helloTracerSpecularShadow.bmp");
    world.Release();

    cout << "Raytracing Completed" << endl;
    return 1;
}
